#define _GNU_SOURCE
// fix for arm-none-linux-gnueabi
#define __NO_STRING_INLINES

#include <sys/types.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
#include <sys/select.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <time.h>
#include <poll.h>
#include <dirent.h>
#include <signal.h>
#include <dlfcn.h>
#include <fcntl.h>


#include <errno.h>

// static int (*o_bind)(int sockfd,const struct sockaddr *addr,socklen_t addrlen);
int (*o_access)(const char *path, int amode);
int (*o_fclose)(FILE *stream);
int (*o_fflush)(FILE *stream);
// int (*o_fgetc)(FILE *stream);
char *(*o_fgets)(char *string, int n, FILE *stream);
FILE *(*o_fopen)(const char *pathname, const char *mode);
// int (*o_fputc)(int c, FILE *stream);
int (*o_fputs)(const char *s, FILE *stream);
size_t (*o_fread)(void *buffer, size_t size, size_t count, FILE *stream);
int (*o_fscanf)(FILE *stream, const char *format, ...);
int (*o_fseek)(FILE *stream, long offset, int whence);
long (*o_ftell)(FILE *stream);
// int (*o_fprintf)(FILE *stream, const char *format, ...);
size_t (*o_fwrite)(const void *ptr, size_t size, size_t nitems,FILE *stream);
// int (*o_getchar)(void); 
// int (*o_printf)(const char *format, ...);
// int (*o_putc)(int c, FILE *stream);
// int (*o_putchar)(int c);
void (*o_rewind)(FILE *stream);
int (*o_setvbuf)(FILE *stream, char *buf, int type, size_t size);
// int (*o_snprintf)(char *s, size_t n, const char *format, ...);
long (*o_strtol)(const char *str, char **endptr, int base);
int (*o_system)(const char *command);
// int (*o_vsprintf)(char *s, const char *format, va_list ap);
int (*o_brk)(void *addr);
int (*o_fstat)(int fd, struct stat *buf);
// int (*o_getrlimit)(int resource, struct rlimit *rlim);
ssize_t (*o_readv)(int fd, const struct iovec *iov, int iovcnt);
ssize_t (*o_writev)(int fd, const struct iovec *iov, int iovcnt);
off_t (*o_lseek)(int fd, off_t offset, int whence);
void *(*o_mmap)(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
int (*o_mprotect)(void *addr, size_t len, int prot);
int (*o_munmap)(void *addr, size_t length);
int (*o_sigaction)(int signum, const struct sigaction *act, struct sigaction *oldact);
int (*o_sigprocmask)(int how, const sigset_t *set, sigset_t *oldset);
int (*o_stat)(const char *path, struct stat *buf);
int (*o_sysinfo)(struct sysinfo *info);
int (*o_tgkill)(int tgid, int tid, int sig);
time_t (*o_time)(time_t *t);
int (*o_unlink)(const char *pathname);
int (*o_accept)(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
int (*o_bind)(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int (*o_close)(int fd);
int (*o_closedir)(DIR *dirp);
int (*o_dup)(int oldfd);
int (*o_dup2)(int oldfd, int newfd);
int (*o_dup3)(int oldfd, int newfd, int flags);
int (*o_fcntl)(int fd, int cmd, ...);
FILE *(*o_fdopen)(int fd, const char *mode);
int (*o_fileno)(FILE *stream);
char *(*o_getpass)( const char *prompt);
int (*o_getsockopt)(int sockfd, int level, int optname, void *optval, socklen_t *optlen);
int (*o_listen)(int sockfd, int backlog);
// int (*o_open)(const char *pathname, int flags);
// int (*o_open64)(const char *pathname, int flags);
DIR *(*o_opendir)(const char *name);
ssize_t (*o_pread)(int fd, void *buf, size_t count, off_t offset);
ssize_t (*o_pwrite)(int fd, const void *buf, size_t count, off_t offset);
ssize_t (*o_pwrite64)(int fd, const void *buf, size_t count, off_t offset);
ssize_t (*o_read)(int fd, void *buf, size_t count);
struct dirent *(*o_readdir)(DIR *dirp);
ssize_t (*o_recv)(int sockfd, void *buf, size_t len, int flags);
ssize_t (*o_recvfrom)(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t *addrlen);
int (*o_select)(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);
int (*o_poll)(struct pollfd *fds, nfds_t nfds, int timeout);
ssize_t (*o_send)(int sockfd, const void *buf, size_t len, int flags);
int (*o_setsockopt)(int sockfd, int level, int optname, const void *optval, socklen_t optlen);
int (*o_socket)(int domain, int type, int protocol);
char *(*o_strdup)(const char *s);
ssize_t (*o_write)(int fd, const void *buf, size_t count);



// interact with PANDA
int abcdefghijklmn(int arg1,int arg2,int arg3,int arg4)
{
    return 0;
}

// // hook bind()
// int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
// {
//     errno = 0;
//     int result;
//     o_bind = dlsym(RTLD_NEXT,"bind");
//     if(o_bind == NULL)
//     {
//         printf("[info] cannot find bind() func\n");
//         return -1;
//     }
//     result = o_bind(sockfd,addr,addrlen);
//     if (errno != 0)
//     {
//         printf("[info] errno: %d\n",errno);
//         abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
//     }
//     return result;
// }


int fscanf(FILE *stream, const char *format, ...)
{
    errno = 0;
    int result;
    o_fscanf = dlsym(RTLD_NEXT,"fscanf");
    va_list args;
    va_start(args, format);
    result = o_fscanf(stream, format, args);
    if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } 
    return result; 
}


// int fprintf(FILE *stream, const char *format, ...)
// {
//     errno = 0;
//     int result;
//     o_fprintf = dlsym(RTLD_NEXT,"fprintf");
//     va_list args;
//     va_start(args, format);
//     result = o_fprintf(stream, format, args);
//     if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } 
//     return result; 
// }


// int printf(const char *format, ...)
// {
//     errno = 0;
//     int result;
//     o_printf = dlsym(RTLD_NEXT,"printf");
//     va_list args;
//     va_start(args, format);
//     result = o_printf(format, args);
//     if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } 
//     return result; 
// }


// int snprintf(char *s, size_t n, const char *format, ...)
// {
//     errno = 0;
//     int result;
//     o_snprintf = dlsym(RTLD_NEXT,"snprintf");
//     va_list args;
//     va_start(args, format);
//     result = o_snprintf(s ,n , format, args);
//     if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } 
//     return result; 
// }


int fcntl(int fd, int cmd, ...)
{
    errno = 0;
    int result;
    o_fcntl = dlsym(RTLD_NEXT,"fcntl");
    va_list args;
    va_start(args, cmd);
    result = o_fcntl(fd, cmd, args);
    if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } 
    return result; 
}
/*
#define HOOK_FUNC(type,name,...) \
type name(__VA_ARGS__) \
{ \
    errno = 0; \
    int result; \
    o_##name = dlsym(RTLD_NEXT,#name); \
    result = o_##name(__VA_ARGS__); \
    if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } \ 
    return result; \
} \
*/

// int open(const char *pathname, int flags) { errno = 0; int result; o_open = dlsym(RTLD_NEXT,"open"); result = o_open(pathname,flags); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// int open64(const char *pathname, int flags) { errno = 0; int result; o_open64 = dlsym(RTLD_NEXT,"open64"); result = o_open64(pathname,flags); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// int getrlimit(int resource, struct rlimit *rlim) { errno = 0; int result; o_getrlimit = dlsym(RTLD_NEXT,"getrlimit"); result = o_getrlimit(resource,rlim); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen) { errno = 0; int result; o_bind = dlsym(RTLD_NEXT,"bind"); result = o_bind(sockfd, addr, addrlen); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int access(const char *path, int amode) { errno = 0; int result; o_access = dlsym(RTLD_NEXT,"access"); result = o_access(path, amode); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int fclose(FILE *stream) { errno = 0; int result; o_fclose = dlsym(RTLD_NEXT,"fclose"); result = o_fclose(stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int fflush(FILE *stream) { errno = 0; int result; o_fflush = dlsym(RTLD_NEXT,"fflush"); result = o_fflush(stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// int fgetc(FILE *stream) { errno = 0; int result; o_fgetc = dlsym(RTLD_NEXT,"fgetc"); result = o_fgetc(stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
char* fgets(char *string, int n, FILE *stream) { errno = 0; char* result; o_fgets = dlsym(RTLD_NEXT,"fgets"); result = o_fgets(string,n,stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
FILE* fopen(const char *pathname, const char *mode) { errno = 0; FILE* result; o_fopen = dlsym(RTLD_NEXT,"fopen"); result = o_fopen(pathname, mode); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// int fputc(int c, FILE *stream) { errno = 0; int result; o_fputc = dlsym(RTLD_NEXT,"fputc"); result = o_fputc(c,stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int fputs(const char *s, FILE *stream) { errno = 0; int result; o_fputs = dlsym(RTLD_NEXT,"fputs"); result = o_fputs(s, stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
size_t fread(void *buffer, size_t size, size_t count, FILE *stream) { errno = 0; size_t result; o_fread = dlsym(RTLD_NEXT,"fread"); result = o_fread(buffer,size,count,stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int fseek(FILE *stream, long offset, int whence) { errno = 0; int result; o_fseek = dlsym(RTLD_NEXT,"fseek"); result = o_fseek(stream, offset, whence); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
long ftell(FILE *stream) { errno = 0; long result; o_ftell = dlsym(RTLD_NEXT,"ftell"); result = o_ftell(stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
size_t fwrite(const void *ptr, size_t size, size_t nitems,FILE *stream) { errno = 0; size_t result; o_fwrite = dlsym(RTLD_NEXT,"fwrite"); result = o_fwrite(ptr, size, nitems,stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// int getchar() { errno = 0; int result; o_getchar = dlsym(RTLD_NEXT,"getchar"); result = o_getchar(); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// int putc(int c, FILE *stream) { errno = 0; int result; o_putc = dlsym(RTLD_NEXT,"putc"); result = o_putc(c, stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// int putchar(int c) { errno = 0; int result; o_putchar = dlsym(RTLD_NEXT,"putchar"); result = o_putchar(c); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
void rewind(FILE *stream) { errno = 0;  o_rewind = dlsym(RTLD_NEXT,"rewind"); o_rewind(stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return ; }
int setvbuf(FILE *stream, char *buf, int type, size_t size) { errno = 0; int result; o_setvbuf = dlsym(RTLD_NEXT,"setvbuf"); result = o_setvbuf(stream,buf,type,size); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
long strtol(const char *str, char **endptr, int base) { errno = 0; long result; o_strtol = dlsym(RTLD_NEXT,"strtol"); result = o_strtol(str,endptr, base); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int system(const char *command) { errno = 0; int result; o_system = dlsym(RTLD_NEXT,"system"); result = o_system(command); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// int vsprintf(char *s, const char *format, va_list ap) { errno = 0; int result; o_vsprintf = dlsym(RTLD_NEXT,"vsprintf"); result = o_vsprintf(s,format,ap); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int brk(void *addr) { errno = 0; int result; o_brk = dlsym(RTLD_NEXT,"brk"); result = o_brk(addr); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int fstat(int fd, struct stat *buf) { errno = 0; int result; o_fstat = dlsym(RTLD_NEXT,"fstat"); result = o_fstat(fd, buf); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
ssize_t readv(int fd, const struct iovec *iov, int iovcnt) { errno = 0; ssize_t result; o_readv = dlsym(RTLD_NEXT,"readv"); result = o_readv(fd,iov,iovcnt); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
ssize_t writev(int fd, const struct iovec *iov, int iovcnt) { errno = 0; ssize_t result; o_writev = dlsym(RTLD_NEXT,"writev"); result = o_writev(fd, iov,iovcnt); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
off_t lseek(int fd, off_t offset, int whence) { errno = 0; off_t result; o_lseek = dlsym(RTLD_NEXT,"lseek"); result = o_lseek(fd, offset, whence); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
void* mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) { errno = 0; void* result; o_mmap = dlsym(RTLD_NEXT,"mmap"); result = o_mmap(addr,length, prot,flags,fd, offset); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int mprotect(void *addr, size_t len, int prot) { errno = 0; int result; o_mprotect = dlsym(RTLD_NEXT,"mprotect"); result = o_mprotect(addr,len,prot); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int munmap(void *addr, size_t length) { errno = 0; int result; o_munmap = dlsym(RTLD_NEXT,"munmap"); result = o_munmap(addr,length); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact) { errno = 0; int result; o_sigaction = dlsym(RTLD_NEXT,"sigaction"); result = o_sigaction(signum,act,oldact); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int sigprocmask(int how, const sigset_t *set, sigset_t *oldset) { errno = 0; int result; o_sigprocmask = dlsym(RTLD_NEXT,"sigprocmask"); result = o_sigprocmask(how,set,oldset); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int stat(const char *path, struct stat *buf) { errno = 0; int result; o_stat = dlsym(RTLD_NEXT,"stat"); result = o_stat(path, buf); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int sysinfo(struct sysinfo *info) { errno = 0; int result; o_sysinfo = dlsym(RTLD_NEXT,"sysinfo"); result = o_sysinfo(info); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int tgkill(int tgid, int tid, int sig) { errno = 0; int result; o_tgkill = dlsym(RTLD_NEXT,"tgkill"); result = o_tgkill(tgid,tid,sig); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
time_t time(time_t *t) { errno = 0; time_t result; o_time = dlsym(RTLD_NEXT,"time"); result = o_time(t); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int unlink(const char *pathname) { errno = 0; int result; o_unlink = dlsym(RTLD_NEXT,"unlink"); result = o_unlink(pathname); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen) { errno = 0; int result; o_accept = dlsym(RTLD_NEXT,"accept"); result = o_accept(sockfd,addr,addrlen); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int close(int fd) { errno = 0; int result; o_close = dlsym(RTLD_NEXT,"close"); result = o_close(fd); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int closedir(DIR *dirp) { errno = 0; int result; o_closedir = dlsym(RTLD_NEXT,"closedir"); result = o_closedir(dirp); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int dup(int oldfd) { errno = 0; int result; o_dup = dlsym(RTLD_NEXT,"dup"); result = o_dup(oldfd); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int dup2(int oldfd, int newfd) { errno = 0; int result; o_dup2 = dlsym(RTLD_NEXT,"dup2"); result = o_dup2(oldfd,newfd); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int dup3(int oldfd, int newfd, int flags) { errno = 0; int result; o_dup3 = dlsym(RTLD_NEXT,"dup3"); result = o_dup3(oldfd,newfd,flags); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
FILE* fdopen(int fd, const char *mode) { errno = 0; FILE* result; o_fdopen = dlsym(RTLD_NEXT,"fdopen"); result = o_fdopen(fd,mode); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int fileno(FILE *stream) { errno = 0; int result; o_fileno = dlsym(RTLD_NEXT,"fileno"); result = o_fileno(stream); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
char* getpass(const char *prompt) { errno = 0; char* result; o_getpass = dlsym(RTLD_NEXT,"getpass"); result = o_getpass(prompt); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int getsockopt(int sockfd, int level, int optname, void *optval, socklen_t *optlen) { errno = 0; int result; o_getsockopt = dlsym(RTLD_NEXT,"getsockopt"); result = o_getsockopt(sockfd,level,optname,optval,optlen); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int listen(int sockfd, int backlog) { errno = 0; int result; o_listen = dlsym(RTLD_NEXT,"listen"); result = o_listen(sockfd,backlog); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
DIR* opendir(const char *name) { errno = 0; DIR* result; o_opendir = dlsym(RTLD_NEXT,"opendir"); result = o_opendir(name); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
ssize_t pread(int fd, void *buf, size_t count, off_t offset) { errno = 0; ssize_t result; o_pread = dlsym(RTLD_NEXT,"pread"); result = o_pread(fd,buf,count,offset); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
ssize_t pwrite(int fd, const void *buf, size_t count, off_t offset) { errno = 0; ssize_t result; o_pwrite = dlsym(RTLD_NEXT,"pwrite"); result = o_pwrite(fd, buf,count,offset); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
// ssize_t pwrite64(int fd, const void *buf, size_t count, off_t offset) { errno = 0; ssize_t result; o_pwrite64 = dlsym(RTLD_NEXT,"pwrite64"); result = o_pwrite64(fd,buf,count,offset); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
ssize_t read(int fd, void *buf, size_t count) { errno = 0; ssize_t result; o_read = dlsym(RTLD_NEXT,"read"); result = o_read(fd, buf,count); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
struct dirent *readdir(DIR *dirp) { errno = 0; struct dirent *result; o_readdir = dlsym(RTLD_NEXT,"*readdir"); result = o_readdir(dirp); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
ssize_t recv(int sockfd, void *buf, size_t len, int flags) { errno = 0; ssize_t result; o_recv = dlsym(RTLD_NEXT,"recv"); result = o_recv(sockfd,buf,len,flags); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags, struct sockaddr *src_addr, socklen_t *addrlen) { errno = 0; ssize_t result; o_recvfrom = dlsym(RTLD_NEXT,"recvfrom"); result = o_recvfrom(sockfd,buf,len,flags, src_addr, addrlen); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout) { errno = 0; int result; o_select = dlsym(RTLD_NEXT,"select"); result = o_select(nfds, readfds,writefds,exceptfds,timeout); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int poll(struct pollfd *fds, nfds_t nfds, int timeout) { errno = 0; int result; o_poll = dlsym(RTLD_NEXT,"poll"); result = o_poll(fds,nfds,timeout); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
ssize_t send(int sockfd, const void *buf, size_t len, int flags) { errno = 0;ssize_t result; o_send = dlsym(RTLD_NEXT,"send"); result = o_send(sockfd,buf,len,flags); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int setsockopt(int sockfd, int level, int optname, const void *optval, socklen_t optlen) { errno = 0; int result; o_setsockopt = dlsym(RTLD_NEXT,"setsockopt"); result = o_setsockopt(sockfd,level,optname,optval,optlen); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
int socket(int domain, int type, int protocol) { errno = 0; int result; o_socket = dlsym(RTLD_NEXT,"socket"); result = o_socket(domain,type,protocol); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }

char* strdup(const char *s)
{ 
    errno = 0; 
    char *result; 
    o_strdup = dlsym(RTLD_NEXT,"strdup"); 
    result = o_strdup(s); 
    if(errno != 0) 
    {
        abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); 
    }
     return result; 
}
ssize_t write(int fd, const void *buf, size_t count) { errno = 0; ssize_t result; o_write = dlsym(RTLD_NEXT,"write"); result = o_write(fd, buf,count); if(errno != 0) { abcdefghijklmn(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno); } return result; }
