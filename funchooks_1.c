#define _GNU_SOURCE
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <errno.h>


int (*realopen)(const char *pathname, int flags);
int (*realopen64)(const char *pathname, int flags);
int (*o_xstat)(int ver, const char * path, struct stat * stat_buf);
int (*o_lxstat)(int ver, const char * path, struct stat * stat_buf);
int (*o_fxstat)(int ver, int fildes, struct stat * stat_buf);
int (*o_xstat64)(int ver, const char * path, struct stat64 * stat_buf);
int (*o_lxstat64)(int ver, const char * path, struct stat64 * stat_buf);
int (*o_fxstat64)(int ver, int fildes, struct stat64 * stat_buf);


int namedoesntmatter(int arg1,int arg2,int arg3,int arg4)
{
    return 0;
}


int open(const char *pathname, int flags) {

    errno = 0;
    int result;
    realopen = dlsym(RTLD_NEXT, "open");
    result = realopen(pathname, flags);
    if (errno != 0)
    {
        namedoesntmatter(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
    }
    return result;
}

int open64(const char *pathname, int flags) {

    errno = 0;
    int result;
    realopen64 = dlsym(RTLD_NEXT, "open64");
    result = realopen64(pathname, flags);
    if (errno != 0)
    {
        namedoesntmatter(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
    }
    return result;
}

int __xstat(int ver, const char * path, struct stat * stat_buf)
{
    errno = 0;
    int result;
    o_xstat = dlsym(RTLD_NEXT, "__xstat");
    result = o_xstat(ver,path,stat_buf);
    if (errno != 0)
    {
        namedoesntmatter(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
    }
    return result;
}

int __lxstat(int ver, const char * path, struct stat * stat_buf)
{
    errno = 0;
    int result;
    o_lxstat = dlsym(RTLD_NEXT, "__lxstat");
    result = o_lxstat(ver,path,stat_buf);
    if (errno != 0)
    {
        namedoesntmatter(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
    }
    return result;
}

int __fxstat(int ver, int fildes, struct stat * stat_buf)
{
    errno = 0;
    int result;
    o_fxstat = dlsym(RTLD_NEXT, "__fxstat");
    result = o_fxstat(ver,fildes,stat_buf);
    if (errno != 0)
    {
        namedoesntmatter(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
    }
    return result;
}

int __xstat64(int ver, const char * path, struct stat64 * stat_buf)
{
    errno = 0;
    int result;
    o_xstat64 = dlsym(RTLD_NEXT, "__xstat64");
    result = o_xstat64(ver,path,stat_buf);
    if (errno != 0)
    {
        namedoesntmatter(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
    }
    return result;
}

int __lxstat64(int ver, const char * path, struct stat64 * stat_buf)
{
    errno = 0;
    int result;
    o_lxstat64 = dlsym(RTLD_NEXT, "__lxstat64");
    result = o_lxstat64(ver,path,stat_buf);
    if (errno != 0)
    {
        namedoesntmatter(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
    }
    return result;
}

int __fxstat64(int ver, int fildes, struct stat64 * stat_buf)
{
    errno = 0;
    int result;
    o_fxstat64 = dlsym(RTLD_NEXT, "__fxstat64");
    result = o_fxstat64(ver,fildes,stat_buf);
    if (errno != 0)
    {
        namedoesntmatter(0xdeadbeef,0xdeadbeef,0xdeadbeef,errno);
    }
    return result;
}